@extends('Layouts/app')
@section('title')
    Login Page
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-md-4 col-md-4">
                <h1>Login</h1>
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('errorLog'))
                    <div class="alert alert-danger">
                        {{ session('errorLog') }}
                    </div>

                @endif
                <form action="{{ route('Login') }}" method="post">
                    @csrf
                    @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                    </div>
                    <button type="submit" class="btn btn-primary mb-4">Login</button>
                </form>
            </div>
        </div>
    </div>
@endsection
