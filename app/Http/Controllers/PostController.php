<?php

namespace App\Http\Controllers;

use App\Post;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PostController extends Controller
{
public function CreatePost( Request $request){
     // Validation
    $request->validate([
       'title'        => 'required|min:3|max:55',
       'description'  => 'required|min:15|max:500',
       'image'        => 'required|image|mimes:jpg,png,jpeg,gif,svg',
    ]);
    // Creat Post
     Post::create([
         'title'       =>  $request->input('title'),
         'description' =>  $request->input('description'),
         'image'       =>  $request->file('image')->store('uploads','public'),
         'user_id'     =>  auth()->user()->id,
     ]);

    return redirect()->route('Post');
}
public function paginate(){
    $data =  Post::orderBy('created_at', 'desc')->paginate(3);
    return view('Post')->with([ 'data' => $data ]);
}
}

