<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\User\CreateUserRequest;
class LoginController extends Controller
{
//    User Login
    public function index(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $created = $request->only('email','password');
        if (Auth::attempt($created)) {
            // Authentication passed...
            return redirect()->route('Profile');
        }else{
            return redirect()->back()->with('errorLog','Email-Address or Password Are Wrong.');
        }
    }
//    User LogOut
    public function Logout(){
        Auth::logout();
        return redirect()->route( 'Login_page');
    }
}
